from selenium import webdriver
import time

driver = webdriver.Chrome('/usr/local/bin/chromedriver')

driver.maximize_window()

driver.get("https://limehome-qa-task.herokuapp.com/")

driver.find_element_by_id("mat-input-0").click()  #click on last name field

driver.find_element_by_id("mat-input-0").send_keys("test name") #enter a test last name

driver.find_element_by_id("mat-input-1").click()  #click on booking reference field

driver.find_element_by_id("mat-input-1").send_keys("123456") #enter a test booking number

time.sleep(1)

driver.find_element_by_class_name("mat-button-wrapper").click() #click on submit button

time.sleep(2)

textbox = driver.find_element_by_id("mat-input-3").get_attribute('value') #retrieve the value in the last name field

if textbox == "test name":                       #check if last name in the previous screen is equal to the form screen
    print("both last names are the same")
else:
    print("last names are not the same")

driver.close()