Task 1:

Test Scenarios:

1.	Verify that the first screen in the online check-in service is having options to add the last name and the booking reference
2.	Verify that the user is able to retrieve the reservation details with valid last name and booking reference 
3.	Verify that clicking the submit button after filling the mandatory fields sends the data to the server 
4.	Verify that the user is not able to view any reservation details with either an invalid last name or an invalid booking reference 
5.	Verify that the required fields are marked with * (in both the first screen and the personal details form)
6.	Verify that a missing mandatory field label is displayed if the user does not enter any of the two mandatory fields 
7.	Check if the reservation details are displayed for the same customer and not any other 
8.	Check validation on booking reference field by entering invalid characters (alphabets and special characters)
9.	Verify that entering blank spaces in mandatory fields leads to a validation error 
10.	Verify that the passport picture is uploaded successfully 
11.	Verify that the user is asked to upload the passport picture and enter the passport ID if the nationality is any other than German 
12.	Verify that the user is not asked for the passport ID and picture if the nationality is German
13.	Verify that only image files (jpg, png) are allowed to be selected when uploading the passport picture 
14.	Check validation constraint on date field in the personal details form (only valid dates should be allowed)
15.	Check validation rules and constraints on Postal Code, Street Address and City fields in the personal details form (by entering invalid characters)
16.	Verify that the data is successfully transmitted after clicking the submit button on the form screen 
17.	Verify that the data is not submitted if any of the mandatory fields are missing and a “missing data/field” message is displayed 
18.	Verify that the web app works on mobile browsers 
19.	Verify that the text language (including the field labels in the form) is available in both English and German
20.	Verify that the language of the text changes upon clicking the respective language button 







Task 2: 

Defects:

1.	Clicking the language button on the first screen does not change the language of the text
2.	Clicking the language button on the form screen changes only the reservation details text and not the text of the rest of the page including the personal details form
3.	No validation rule has been applied to the “Passport ID” field in the Personal Details form (No character limit, accepts special characters)  
4.	No validation check on the date in the “Date of Birth” field in the Personal Details form (should be restricted to 18 years) 


Task 3:

Defect Ticket:

Defect ID:	001 

Defect Name:	No language change upon clicking the language button 

Report date:	05-01-2020

Reported by:	Ahsan Zameer

URL: 	https://limehome-qa-task.herokuapp.com/

Browser: 	Chrome

Severity:	Low

Priority: 	High

Assigned to:	N/A

Status: 	Open

Description:	Clicking the language change button does not change the language of the text from English to German

Steps to reproduce:	
•	Navigate to https://limehome-qa-task.herokuapp.com/
•	Click the „DE“ language change button on the top right of the page 

Expected Result: 	The entire text on the page, including the input field placeholders should be translated to German upon clicking the button 

Actual Result: 	The language of the text does not change upon clicking the button 


Task 4:

https://gitlab.com/ahsan.zameer93/qa-challenge-limehome/blob/master/AutomationScript.py

 
